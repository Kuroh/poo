import java.security.spec.RSAOtherPrimeInfo;

class Carnet {
    String dni;
    String nombres;
    String fechaExpiracion;
    float ancho;
    double peso;
    String color;
    String codigo;

    public void imprimir(){
        System.out.println("Nombres:"+nombres);
        System.out.println("DNI:"+dni);
    }

    public static void main(String[] args) {
        Carnet carnet = new Carnet();
        carnet.dni="73012444";
        carnet.nombres="Jake Robinson Anghelo";
        carnet.imprimir();
        Carnet carnet1=carnet;
        carnet1.dni="3242434";
        carnet1.imprimir();
        carnet.imprimir();

    }
}
