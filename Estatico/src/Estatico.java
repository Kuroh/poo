public class Estatico {
    private static int a=10;

    public static int suma(int a, int b){
        int suma=a+b;
        return suma;
    }

    //Los atributos y métodos estaticos le pertenecen a la clase en sí, no al objeto
    //Al ser modificados en el main, se modifica para todos los objetos creados en la clase

    public static void main(String[] args) {
        System.out.println(Estatico.a);
        System.out.println(Estatico.suma(4,5));
    }
}
