package Polimorph;
public class Principal {
    public static void main(String[] args) {
        Vehiculo[] misVehiculos=new Vehiculo[4];

        misVehiculos[0]=new Vehiculo("GH67", "Ferrari", "A89");
        misVehiculos[1]=new VehiculoTurismo("78HY", "Audi", "A12", 4);
        misVehiculos[2]=new VehiculoDeportivo("23HE", "Toyota", "B45", 20);
        misVehiculos[3]=new VehiculoFurgoneta("DJ21", "Mitsubishi", "J10", 1000);

        for(Vehiculo vehiculos: misVehiculos){
            System.out.println(vehiculos.mostrarDatos());
            System.out.println("-----------------");
        }
    }
}
