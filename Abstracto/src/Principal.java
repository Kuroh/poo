public class Principal {
    public static void main(String[] args) {
        Planta planta=new Planta();
        AnimalCarnivoro carnivoro= new AnimalCarnivoro();

        planta.alimentarse();
        carnivoro.alimentarse();
    }
}
