create table usuario(
    idUsuario varchar(200),
    credencial varchar(200),
    nomUsuario varchar(200),
    apellUsuario varchar(200),
    estadoUsuario varchar(200),
    primary key (idUsuario)
);

insert into usuario (idUsuario, credencial, nomUsuario, apellUsuario, estadoUsuario)
values ('1','AB12','Jake','Tello','Disponible'),
       ('2','CD34','José','Vargas','Ocupado');

select *
from usuario where idUsuario=1 and credencial='AB12';

select idUsuario,
       credencial,
       nomUsuario,
       apellUsuario,
       estadoUsuario from usuario
where idUsuario=1 and credencial='AB12';

select *
from usuario;

create table Producto(
    idProducto varchar(200),
    nomProducto varchar(200),
    estadoProducto varchar(200)
);

insert into Producto (idProducto, nomProducto, estadoProducto)
values ('1','Marcador Vinifan A1','1'),
       ('2','Marcador Vinifan A2','1'),
       ('3','Marcador Vinifan A3','1'),
       ('4','Marcador Vinifan A4','0'),
       ('5','Marcador Vinifan A5','1');

select *
from Producto;

select idProducto,nomProducto,estadoProducto
from Producto
where estadoProducto='1';