package pe.edu.fiis.poo.demo.util;

import pe.edu.fiis.poo.demo.domain.Usuario;
import pe.edu.fiis.poo.demo.dto.UsuarioRequest;

public class DemoUtil {
    public static Usuario mapToUsuario(UsuarioRequest request){
        Usuario response =new Usuario();
        response.setCredencial(request.getCredencial());
        response.setIdUsuario(request.getIdUsuario());
        response.setNomUsuario(request.getNomUsuario());
        response.setApellUsuario(request.getApellUsuario());
        response.setEstadoUsuario(request.getEstadoUsuario());
        return response;
    }
}
