package pe.edu.fiis.poo.demo.dao.mapper;

import pe.edu.fiis.poo.demo.domain.Producto;

import java.sql.ResultSet;
import java.sql.SQLException;

public class ProductoMapper {
    public Producto mapRow(ResultSet resultSet, int i) throws SQLException {
        Producto response= new Producto();
        response.setIdProducto(resultSet.getString("idUsuario"));
        response.setNomProducto(resultSet.getString("nomUsuario"));
        response.setEstadoProducto(resultSet.getString("estadoUsuario"));
        return response;
    }
}
