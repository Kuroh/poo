package pe.edu.fiis.poo.demo.dto;

import pe.edu.fiis.poo.demo.domain.Venta;

import java.util.List;

public class VentaResponse {
    private List<Venta> lista;

    public List<Venta> getLista() {
        return lista;
    }

    public void setLista(List<Venta> lista) {
        this.lista = lista;
    }
}
