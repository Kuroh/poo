package pe.edu.fiis.poo.demo.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pe.edu.fiis.poo.demo.dao.MyDao;
import pe.edu.fiis.poo.demo.domain.Producto;
import pe.edu.fiis.poo.demo.domain.Usuario;
import pe.edu.fiis.poo.demo.domain.Venta;
import pe.edu.fiis.poo.demo.dto.UsuarioRequest;
import pe.edu.fiis.poo.demo.dto.UsuarioResponse;
import pe.edu.fiis.poo.demo.util.DemoUtil;

import java.util.List;

@Service
public class MyServiceImpl implements MyService{

    @Autowired
    private MyDao myDao ;

    public UsuarioResponse loginUsuario(UsuarioRequest request){
        Usuario usuario =this.myDao.loginUsuario(DemoUtil.mapToUsuario(request));
        UsuarioResponse response=new UsuarioResponse();
        response.setUsuario(usuario);
        return response;
    }

    @Override
    public UsuarioResponse actualizarCredencial(UsuarioRequest request) {
        Usuario usuario=this.myDao.actualizarCredencial(DemoUtil.mapToUsuario(request));
        UsuarioResponse response=new UsuarioResponse();
        response.setUsuario(usuario);
        return response;
    }

    @Override
    public UsuarioResponse removerUsuario(UsuarioRequest request) {
        Usuario usuario=this.myDao.removerUsuario(DemoUtil.mapToUsuario(request));
        UsuarioResponse response=new UsuarioResponse();
        response.setUsuario(usuario);
        return response;
    }

    @Override
    public UsuarioResponse crearUsuario(UsuarioRequest request) {
        Usuario usuario=this.myDao.crearUsuario(DemoUtil.mapToUsuario(request));
        UsuarioResponse response=new UsuarioResponse();
        response.setUsuario(usuario);
        return response;
    }


    public List<Producto> obtenerProductos(){
        return this.myDao.obtenerProductos();
    }
    public List<Venta> obtenerVentasGrandes(){
        return this.myDao.obtenerVentasGrandes();
    }

}
