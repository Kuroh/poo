package pe.edu.fiis.poo.demo.dao;

import pe.edu.fiis.poo.demo.domain.Producto;
import pe.edu.fiis.poo.demo.domain.Usuario;
import pe.edu.fiis.poo.demo.domain.Venta;

import java.util.List;

public interface MyDao {
    Usuario loginUsuario(Usuario request);
    List<Producto> obtenerProductos();
    List<Venta> obtenerVentasGrandes();
    Usuario actualizarCredencial(Usuario request);
    Usuario removerUsuario(Usuario request);
    Usuario crearUsuario(Usuario request);
}
