package pe.edu.fiis.poo.demo.dao;

import org.springframework.stereotype.Repository;
import pe.edu.fiis.poo.demo.dao.datasource.MyDatasource;
import pe.edu.fiis.poo.demo.dao.mapper.UsuarioMapper;
import pe.edu.fiis.poo.demo.domain.Producto;
import pe.edu.fiis.poo.demo.domain.Usuario;
import pe.edu.fiis.poo.demo.domain.Venta;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Repository
public class MyDaoImpl extends MyDatasource implements MyDao {

    public Usuario loginUsuario(Usuario request){
        Usuario usuario=null;
        try{
            usuario= (Usuario) this.jdbcTemplate.queryForObject(" select idUsuario,"+
                            "credencial,"+
                            "nomUsuario,"+
                            "apellUsuario,"+
                            "estadoUsuario from usuario "+
                            "where idUsuario=?"+" and credencial=?", new String[]{request.getIdUsuario(),
                            request.getCredencial()},
                    new UsuarioMapper());
        }catch (Exception ex){
            return usuario;
        }
        return usuario;
    }

    public Usuario actualizarCredencial(Usuario request){
        String sql="update usuario "+
                "set credencial=? "+
                "where idUsuario=?";
        this.jdbcTemplate.update(sql,new String[]{request.getCredencial(),request.getIdUsuario()});
        return request;
    }

    public Usuario removerUsuario(Usuario request){
        String sql="delete from usuario "+
                "where idUsuario=?";
        this.jdbcTemplate.update(sql,new String[]{request.getIdUsuario()});
        return request;
    }

    public Usuario crearUsuario(Usuario request){
        String sql="insert into usuario (idUsuario,"+
                "  credencial,"+
                "  nomUsuario,"+
                "  apellUsuario,"+
                "  estadoUsuario)"+
                "  values(?,?,?,?,?)";
        this.jdbcTemplate.update(sql,new String[]{request.getIdUsuario(),request.getCredencial(),
                                                    request.getNomUsuario(),request.getApellUsuario(),"ACTIVE"});
        return request;
    }

    @Override
    public List<Producto> obtenerProductos() {
        List<Producto> lista =null;
        try{
            List<Map<String,Object>> filas=this.jdbcTemplate.queryForList(
                    "select idProducto,nomProducto,estadoProducto "+
                            "from Producto "+
                            "where estadoProducto='1'");
            lista=new ArrayList<Producto>();
            for(Map<String,Object> fila:filas){
                Producto p=new Producto();
                p.setIdProducto((String) fila.get("idProducto"));
                p.setNomProducto((String) fila.get("nomProducto"));
                p.setEstadoProducto((String) fila.get("estadoProducto"));
                lista.add(p);
            }
        }catch (Exception ex){
            return lista;
        }
        return lista;
    }

    @Override
    public List<Venta> obtenerVentasGrandes() {
        List<Venta> lista=null;
        try{
            List<Map<String, Object>> filas=this.jdbcTemplate.queryForList(
                    "select idVenta,montoVenta "+
                            "from Venta "+
                            "where montoVenta>1000");
            lista =new ArrayList<Venta>();
            for(Map<String,Object> fila:filas){
                Venta v=new Venta();
                v.setIdVenta((Integer) fila.get("idVenta"));
                v.setMontoVenta((Double) fila.get("montoVenta"));
                lista.add(v);
            }
        }catch (Exception ex){
            return lista;
        }
        return lista;
    }
}
