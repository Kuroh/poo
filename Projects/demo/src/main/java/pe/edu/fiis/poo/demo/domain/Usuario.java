package pe.edu.fiis.poo.demo.domain;

public class Usuario {
    private String idUsuario;
    private String credencial;
    private String nomUsuario;
    private String apellUsuario;
    private String estadoUsuario;

    public String getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(String idUsuario) {
        this.idUsuario = idUsuario;
    }

    public String getCredencial() {
        return credencial;
    }

    public void setCredencial(String credencial) {
        this.credencial = credencial;
    }

    public String getNomUsuario() {
        return nomUsuario;
    }

    public void setNomUsuario(String nomUsuario) {
        this.nomUsuario = nomUsuario;
    }

    public String getApellUsuario() {
        return apellUsuario;
    }

    public void setApellUsuario(String apellUsuario) {
        this.apellUsuario = apellUsuario;
    }

    public String getEstadoUsuario() {
        return estadoUsuario;
    }

    public void setEstadoUsuario(String estadoUsuario) {
        this.estadoUsuario = estadoUsuario;
    }
}
