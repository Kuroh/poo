package pe.edu.fiis.poo.demo.service;

import pe.edu.fiis.poo.demo.domain.Producto;
import pe.edu.fiis.poo.demo.domain.Venta;
import pe.edu.fiis.poo.demo.dto.UsuarioRequest;
import pe.edu.fiis.poo.demo.dto.UsuarioResponse;

import java.util.List;

public interface MyService {
    UsuarioResponse loginUsuario(UsuarioRequest request);
    UsuarioResponse actualizarCredencial(UsuarioRequest request);
    UsuarioResponse removerUsuario(UsuarioRequest request);
    UsuarioResponse crearUsuario(UsuarioRequest request);
    public List<Producto> obtenerProductos();
    public List<Venta> obtenerVentasGrandes();
}
