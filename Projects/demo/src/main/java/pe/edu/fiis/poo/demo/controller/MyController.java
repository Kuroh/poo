package pe.edu.fiis.poo.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import pe.edu.fiis.poo.demo.dto.ProductoResponse;
import pe.edu.fiis.poo.demo.dto.UsuarioRequest;
import pe.edu.fiis.poo.demo.dto.UsuarioResponse;
import pe.edu.fiis.poo.demo.dto.VentaResponse;
import pe.edu.fiis.poo.demo.service.MyService;

@RestController
public class MyController {

    @Autowired
    private MyService myService ;

    @PostMapping(value = "/usuario",consumes = MediaType.APPLICATION_JSON_VALUE,
                                    produces = MediaType.APPLICATION_JSON_VALUE)
    public UsuarioResponse loginUsuario(@RequestBody UsuarioRequest request){
        UsuarioResponse response = this.myService.loginUsuario(request);
        if(response.getUsuario()==null){
            response.setError("No hay usuario");
        }
        return response;
    }

    @PostMapping(value = "/actualizarUsuario",consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public UsuarioResponse actualizarCredencial(@RequestBody UsuarioRequest request){
        UsuarioResponse response = this.myService.actualizarCredencial(request);
        return response;
    }

    @PostMapping(value = "/eliminarUsuario",consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public UsuarioResponse removerUsuario(@RequestBody UsuarioRequest request){
        UsuarioResponse response = this.myService.removerUsuario(request);
        return response;
    }

    @PostMapping(value = "/crearUsuario",consumes = MediaType.APPLICATION_JSON_VALUE,
                produces = MediaType.APPLICATION_JSON_VALUE)
    public UsuarioResponse crearUsuario(@RequestBody UsuarioRequest request){
        UsuarioResponse response=this.myService.crearUsuario(request);
        return response;
    }

    @PostMapping(value = "/productos",
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ProductoResponse listarProductos(){
        ProductoResponse response =new ProductoResponse();
        response.setLista(this.myService.obtenerProductos());
        return response;
    }

    @PostMapping(value = "/montos",
            produces = MediaType.APPLICATION_JSON_VALUE)
    public VentaResponse mostrarVentasGrandes(){
        VentaResponse response=new VentaResponse();
        response.setLista(this.myService.obtenerVentasGrandes());
        return response;
    }
}
