package pe.edu.fiis.poo.demo.dao.mapper;

import org.springframework.jdbc.core.RowMapper;
import pe.edu.fiis.poo.demo.domain.Usuario;

import java.sql.ResultSet;
import java.sql.SQLException;

public class UsuarioMapper implements RowMapper {
    @Override
    public Usuario mapRow(ResultSet resultSet, int i) throws SQLException {
        Usuario response= new Usuario();
        response.setIdUsuario(resultSet.getString("idUsuario"));
        response.setCredencial(resultSet.getString("credencial"));
        response.setNomUsuario(resultSet.getString("nomUsuario"));
        response.setApellUsuario(resultSet.getString("apellUsuario"));
        response.setEstadoUsuario(resultSet.getString("estadoUsuario"));
        return response;
    }
}
