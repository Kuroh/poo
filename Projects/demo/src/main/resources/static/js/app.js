var app = new Vue({
    el: '#content',
    data: {
        usuario:"Jake",
        clave:null,
        libros: [
            {
                isbn:"001",
                titulo:"titulo 1",
                editorial:"editorial"
            },
            {
                isbn:"002",
                titulo:"titulo 2",
                editorial:"editorial2"
            }
        ]
    },
    methods:{
        cambiar:function () {
            this.usuario="Robinson"
        }
    }
})