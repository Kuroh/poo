package exception;

public class MoneyException extends Exception {
    public MoneyException(String msg){
        super(msg);
    }
}
