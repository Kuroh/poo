package service;

import exception.MoneyException;
import exception.StockException;

public interface RealizarCompra {
    default void realizarTransaccion(){}
    default void confirmarTransaccion() throws StockException, MoneyException {}
}
