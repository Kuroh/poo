package domain;

public class Contacto extends Cliente{
    private String idContacto;
    private String numContacto;

    public Contacto(){}

    public Contacto(String idCliente, String nomCliente, String apellCliente, String dirCliente,
                    Integer dniCliente, String idContacto, String numContacto) {
        super(idCliente, nomCliente, apellCliente, dirCliente, dniCliente);
        this.idContacto = idContacto;
        this.numContacto = numContacto;
    }

    public String getIdContacto() {
        return idContacto;
    }

    public void setIdContacto(String idContacto) {
        this.idContacto = idContacto;
    }

    public String getNumContacto() {
        return numContacto;
    }

    public void setNumContacto(String numContacto) {
        this.numContacto = numContacto;
    }
}
