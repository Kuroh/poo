package domain;

import exception.StockException;
import service.RealizarCompra;

public class ProductoVenta extends Producto implements RealizarCompra {
    private Integer ordenProducto;
    private Integer cantidadProducto;

    public ProductoVenta(){
        super();
    }

    public ProductoVenta(String idProducto, String nomProducto, String descProducto, String tipoProducto,
                         Integer stockProducto, Integer costoProducto, Integer ordenProducto,
                         Integer cantidadProducto) {
        super(idProducto, nomProducto, descProducto, tipoProducto, stockProducto, costoProducto);
        this.ordenProducto = ordenProducto;
        this.cantidadProducto = cantidadProducto;
    }

    public Integer getOrdenProducto() {
        return ordenProducto;
    }

    public void setOrdenProducto(Integer ordenProducto) {
        this.ordenProducto = ordenProducto;
    }

    public Integer getCantidadProducto() {
        return cantidadProducto;
    }

    public void setCantidadProducto(Integer cantidadProducto) {
        this.cantidadProducto = cantidadProducto;
    }

    public int generarMonto(){
        return getCantidadProducto() * getCostoProducto();
    }

    @Override
    public void confirmarTransaccion() throws StockException {
        if(getStockProducto()<getCantidadProducto()){
            throw new StockException("Cantidad de artículos no disponible para su venta.");
        }
        setStockProducto(getStockProducto()-getCantidadProducto());
    }
}
