package domain;

import exception.MoneyException;
import exception.VerificationException;
import service.Generador;
import service.RealizarCompra;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Venta extends Tarjeta implements RealizarCompra , Generador {
    private String idVenta;
    private String fechaTransaccion;
    private Double montoTotal;

    public Venta(){}

    public Venta(String idCliente, String nomCliente, String apellCliente, String dirCliente, Integer dniCliente,
                 String idTarjeta, Integer claveTarjeta, String bancoTarjeta, Double saldoTarjeta, String idVenta,
                 String fechaTransaccion) {
        super(idCliente, nomCliente, apellCliente, dirCliente, dniCliente, idTarjeta, claveTarjeta, bancoTarjeta, saldoTarjeta);
        this.idVenta = idVenta;
        this.fechaTransaccion = fechaTransaccion;
    }

    public String getIdVenta() {
        return idVenta;
    }

    public void setIdVenta(String idVenta) {
        this.idVenta = idVenta;
    }

    public String getFechaTransaccion() {
        return fechaTransaccion;
    }

    public void setFechaTransaccion(String fechaTransaccion) {
        this.fechaTransaccion = fechaTransaccion;
    }

    public Double getMontoTotal() {
        return montoTotal;
    }

    public void setMontoTotal(){
        Double n=0.0;
        for (int i=0;i<body.size();i++){
            n+=body.get(i).generarMonto();
        }
        this.montoTotal=n;
    }

    List<ProductoVenta> body = new ArrayList<ProductoVenta>();
    Scanner lector = new Scanner(System.in);

    @Override
    public void realizarTransaccion() {
        ProductoVenta producto = new ProductoVenta();
        int n=1;
        while(n==1){
            System.out.println("Ingrese ID del producto: "); producto.setIdProducto(lector.nextLine());
            System.out.println("Ingrese cantidad del producto: "); producto.setCantidadProducto(lector.nextInt());
            body.add(producto);
            System.out.println("¿Desea realizar otra adición al carrito de compras?");
            System.out.println("1.Si");
            System.out.println("2.No"); n=lector.nextInt();
        }
    }

    @Override
    public void confirmarTransaccion() throws MoneyException {
        int sum=0;
        for(int i=0;i<body.size();i++){
            sum=body.get(i).generarMonto();
        }
        if(sum>getSaldoTarjeta()){
            throw new MoneyException("Saldo insuficiente.");
        }
        else setSaldoTarjeta(getSaldoTarjeta()-sum);
    }

    @Override
    public void generate() {
        if(body.size()==0) System.out.println("No ha agregado ningún producto al carrito de compras.");
        else{
            System.out.println("GENERADOR DE PREVENTA");
            for (int i=0;i<body.size();i++){
                System.out.println((i+1)+". "+body.get(i).getIdProducto()+"  Monto: "+body.get(i).generarMonto());
            }
        }
    }

    public void confirmarTarjeta() throws VerificationException {
        Integer test;
        System.out.println("Ingrese clave de la tarjeta: ");
        test=lector.nextInt();
        if (test.compareTo(getClaveTarjeta())!=0){
            throw new VerificationException("La clave que usted ha ingresado es incorrecta.");
        }
        else{
            System.out.println("Clave confirmada correctamente.");
        }
    }
}
