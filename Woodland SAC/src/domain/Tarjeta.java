package domain;

import service.RealizarCompra;

public class Tarjeta extends Cliente {
    private String idTarjeta;
    private Integer claveTarjeta;
    private String bancoTarjeta;
    private Double saldoTarjeta;

    public Tarjeta(){}

    public Tarjeta(String idCliente, String nomCliente, String apellCliente, String dirCliente,
                   Integer dniCliente, String idTarjeta, Integer claveTarjeta, String bancoTarjeta,
                   Double saldoTarjeta) {
        super(idCliente, nomCliente, apellCliente, dirCliente, dniCliente);
        this.idTarjeta = idTarjeta;
        this.claveTarjeta = claveTarjeta;
        this.bancoTarjeta = bancoTarjeta;
        this.saldoTarjeta = saldoTarjeta;
    }

    public String getIdTarjeta() {
        return idTarjeta;
    }

    public void setIdTarjeta(String idTarjeta) {
        this.idTarjeta = idTarjeta;
    }

    public Integer getClaveTarjeta() {
        return claveTarjeta;
    }

    public void setClaveTarjeta(Integer claveTarjeta) {
        this.claveTarjeta = claveTarjeta;
    }

    public String getBancoTarjeta() {
        return bancoTarjeta;
    }

    public void setBancoTarjeta(String bancoTarjeta) {
        this.bancoTarjeta = bancoTarjeta;
    }

    public Double getSaldoTarjeta() {
        return saldoTarjeta;
    }

    public void setSaldoTarjeta(Double saldoTarjeta) {
        this.saldoTarjeta = saldoTarjeta;
    }

}
