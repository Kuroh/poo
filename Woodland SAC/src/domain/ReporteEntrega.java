package domain;

import service.Generador;

public class ReporteEntrega extends Venta implements Generador {
    private String fechaEntrega;
    private String sugerenciaCliente;

    public ReporteEntrega(String idCliente, String nomCliente, String apellCliente, String dirCliente,
                          Integer dniCliente, String idTarjeta, Integer claveTarjeta, String bancoTarjeta,
                          Double saldoTarjeta, String idVenta, String fechaTransaccion, String fechaEntrega,
                          String sugerenciaCliente) {
        super(idCliente, nomCliente, apellCliente, dirCliente, dniCliente, idTarjeta, claveTarjeta, bancoTarjeta,
                saldoTarjeta, idVenta, fechaTransaccion);
        this.fechaEntrega = fechaEntrega;
        this.sugerenciaCliente = sugerenciaCliente;
    }

    public String getFechaEntrega() {
        return fechaEntrega;
    }

    public void setFechaEntrega(String fechaEntrega) {
        this.fechaEntrega = fechaEntrega;
    }

    public String getSugerenciaCliente() {
        return sugerenciaCliente;
    }

    public void setSugerenciaCliente(String sugerenciaCliente) {
        this.sugerenciaCliente = sugerenciaCliente;
    }

    public void generate(){
        System.out.println("REPORTE DE ENTREGA A DOMICILIO");
        System.out.println("ID de la venta: "+getIdVenta());
        System.out.println("Cliente: "+getNomCliente()+" "+getApellCliente());
        System.out.println("Con número de tarjeta: "+getIdTarjeta());
        System.out.println("Fecha de realización de compra online: "+getFechaTransaccion());
        System.out.println("Fecha de entrega a domicilio: "+getFechaEntrega());
        System.out.println("Monto total: "+getMontoTotal());
        System.out.println("");
        System.out.println("Gracias por preferir Woodland SAC para su compra :D");
    }

}
