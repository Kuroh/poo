package domain;

public class Producto {
    private String idProducto;
    private String nomProducto;
    private String descProducto;
    private String tipoProducto;
    private Integer stockProducto;
    private Integer costoProducto;

    public Producto(String idProducto, String nomProducto, String descProducto,
                    String tipoProducto, Integer stockProducto, Integer costoProducto) {
        this.idProducto = idProducto;
        this.nomProducto = nomProducto;
        this.descProducto = descProducto;
        this.tipoProducto = tipoProducto;
        this.stockProducto = stockProducto;
        this.costoProducto = costoProducto;
    }

    public Producto() {

    }

    public String getIdProducto() {
        return idProducto;
    }

    public void setIdProducto(String idProducto) {
        this.idProducto = idProducto;
    }

    public String getNomProducto() {
        return nomProducto;
    }

    public void setNomProducto(String nomProducto) {
        this.nomProducto = nomProducto;
    }

    public String getDescProducto() {
        return descProducto;
    }

    public void setDescProducto(String descProducto) {
        this.descProducto = descProducto;
    }

    public String getTipoProducto() {
        return tipoProducto;
    }

    public void setTipoProducto(String tipoProducto) {
        this.tipoProducto = tipoProducto;
    }

    public Integer getStockProducto() {
        return stockProducto;
    }

    public void setStockProducto(Integer stockProducto) {
        this.stockProducto = stockProducto;
    }

    public Integer getCostoProducto() {
        return costoProducto;
    }

    public void setCostoProducto(Integer costoProducto) {
        this.costoProducto = costoProducto;
    }


}
