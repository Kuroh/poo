package domain;

public class Cliente {
    private String idCliente;
    private String nomCliente;
    private String apellCliente;
    private String dirCliente;
    private Integer dniCliente;

    public Cliente(){}

    public Cliente(String idCliente, String nomCliente, String apellCliente, String dirCliente, Integer dniCliente) {
        this.idCliente = idCliente;
        this.nomCliente = nomCliente;
        this.apellCliente = apellCliente;
        this.dirCliente = dirCliente;
        this.dniCliente = dniCliente;
    }

    public String getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(String idCliente) {
        this.idCliente = idCliente;
    }

    public String getNomCliente() {
        return nomCliente;
    }

    public void setNomCliente(String nomCliente) {
        this.nomCliente = nomCliente;
    }

    public String getApellCliente() {
        return apellCliente;
    }

    public void setApellCliente(String apellCliente) {
        this.apellCliente = apellCliente;
    }

    public String getDirCliente() {
        return dirCliente;
    }

    public void setDirCliente(String dirCliente) {
        this.dirCliente = dirCliente;
    }

    public Integer getDniCliente() {
        return dniCliente;
    }

    public void setDniCliente(Integer dniCliente) {
        this.dniCliente = dniCliente;
    }
}
