public class Calculadora {
    private Double primero;
    private Double segundo;
    private Double resultado;

    public void sumar(){
        resultado=primero+segundo;
    }
    public void restar(){
        resultado=primero-segundo;
    }
    public void multiplicar(){
        resultado=primero*segundo;
    }
    public void dividir(){
        resultado=primero/segundo;
    }

    public static void main(String[] args) {
        Calculadora a=new Calculadora();
        a.primero=5.0;
        a.segundo=4.0;
        a.sumar();
        System.out.println(a.resultado);
        a.restar();
        System.out.println(a.resultado);
        a.multiplicar();
        System.out.println(a.resultado);
        a.dividir();
        System.out.println(a.resultado);
    }

}
