public class Transaccion {
    public static void main(String[] args) {
        Producto producto = new Producto(10.50f, 15, "Pilot", "Board Master", "verde");

        /*
        producto.setColor("verde");
        producto.setMarca("Pilot");
        producto.setModelo("Board Master");
        producto.setPrecio(10.50f);
        producto.setStock(15);
        */

        for (int i = 0; i < 10 && producto.getStock()>0; i++) {
            int cantidad = 1 + (int) (Math.random() * 10);
            if (producto.getStock() >= cantidad) {
                Venta venta = new Venta();
                venta.setCantidad(1 + (int) (Math.random() * 10));
                venta.setProducto(producto);
                venta.setIgv(0.18f * venta.getProducto().getPrecio()*venta.getCantidad());
                venta.setMontoTotal(venta.getIgv() + venta.getProducto().getPrecio()*venta.getCantidad());

                venta.imprimir();
                producto.setStock(producto.getStock() - venta.getCantidad());
            }
        }

        Maestro maestro=new Maestro();
        maestro.setNombre("Hancco");

        Docente.cantidad=20;
        Maestro.cantidad=45;
        System.out.println(Docente.imprimir());
        Docente docente=new Docente();
        docente.setEdad(30);
        System.out.println(docente.getEdad());

    }
}
