package pe.edu.uni.fiis.universityhelper.UniversityHelper.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import pe.edu.uni.fiis.universityhelper.UniversityHelper.dto.LibroRequest;
import pe.edu.uni.fiis.universityhelper.UniversityHelper.dto.LibroResponse;
import pe.edu.uni.fiis.universityhelper.UniversityHelper.dto.UsuarioRequest;
import pe.edu.uni.fiis.universityhelper.UniversityHelper.dto.UsuarioResponse;
import pe.edu.uni.fiis.universityhelper.UniversityHelper.service.MyService;

@RestController
public class MyController {
    @Autowired
    private MyService myService;

    @PostMapping(value = "/crearLibro",consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public LibroResponse crearLibro(@RequestBody LibroRequest request){
        LibroResponse response=this.myService.crearLibro(request);
        return response;
    }

    @PostMapping(value = "/eliminarLibro",consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public LibroResponse eliminarLibro(@RequestBody LibroRequest request){
        LibroResponse response=this.myService.eliminarLibro(request);
        return response;
    }

    @PostMapping(value = "/crearUsuario",consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public UsuarioResponse crearUsuario(@RequestBody UsuarioRequest request){
        UsuarioResponse response=this.myService.crearUsuario(request);
        return response;
    }
}
