package pe.edu.uni.fiis.universityhelper.UniversityHelper.util;

import pe.edu.uni.fiis.universityhelper.UniversityHelper.domain.Libro;
import pe.edu.uni.fiis.universityhelper.UniversityHelper.dto.LibroRequest;

public class DemoUtilLibro {
    public static Libro mapToLibro(LibroRequest request){
        Libro response=new Libro();
        response.setCoIsbn(request.getCoIsbn());
        response.setAutorLibro(request.getAutorLibro());
        response.setDescLibro(request.getDescLibro());
        response.setNumPagLibro(request.getNumPagLibro());
        response.setPublicLibro(request.getPublicLibro());
        response.setTituloLibro(request.getTituloLibro());
        return response;
    }
}
