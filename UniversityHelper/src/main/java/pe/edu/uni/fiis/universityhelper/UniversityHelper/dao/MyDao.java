package pe.edu.uni.fiis.universityhelper.UniversityHelper.dao;

import pe.edu.uni.fiis.universityhelper.UniversityHelper.domain.Libro;
import pe.edu.uni.fiis.universityhelper.UniversityHelper.domain.Usuario;

public interface MyDao {
    Libro crearLibro(Libro request);
    Libro eliminarLibro(Libro request);
    Usuario crearUsuario(Usuario request);
}
