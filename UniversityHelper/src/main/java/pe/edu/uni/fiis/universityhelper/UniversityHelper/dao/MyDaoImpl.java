package pe.edu.uni.fiis.universityhelper.UniversityHelper.dao;

import org.springframework.stereotype.Repository;
import pe.edu.uni.fiis.universityhelper.UniversityHelper.dao.datasource.MyDatasource;
import pe.edu.uni.fiis.universityhelper.UniversityHelper.domain.Libro;
import pe.edu.uni.fiis.universityhelper.UniversityHelper.domain.Usuario;

@Repository
public class MyDaoImpl extends MyDatasource implements MyDao {
    @Override
    public Libro crearLibro(Libro request) {
        String sql1="insert into FLibro (coIsbn, tituloLibro, autorLibro, numPagLibro, publicLibro, descLibro, idMaterial)\n" +
                "values (?,?,?,?,?,?,1)";
        this.jdbcTemplate.update(sql1,new Object[]{request.getCoIsbn(),request.getTituloLibro(),
                request.getAutorLibro(), request.getNumPagLibro(),request.getPublicLibro(),
                request.getDescLibro()});

        return request;
    }

    @Override
    public Libro eliminarLibro(Libro request) {
        String sql1="delete from FLibro where coisbn=?";
        this.jdbcTemplate.update(sql1,new Object[]{request.getCoIsbn()});
        return request;
    }

    @Override
    public Usuario crearUsuario(Usuario request) {
        String sql1="insert into FUsuario (idUsuario, nomUsuario, uniUsuario, rolUsuario, contraUsuario, tipoPagoUsuario)\n" +
                "values (?,?,?,'Comprador',?,?)";
        this.jdbcTemplate.update(sql1,new Object[]{request.getIdUsuario(),request.getNomUsuario(),
                request.getUniUsuario(),request.getContraUsuario(),request.getTipoPagoUsuario()});
        return request;
    }
}
