package pe.edu.uni.fiis.universityhelper.UniversityHelper.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pe.edu.uni.fiis.universityhelper.UniversityHelper.dao.MyDao;
import pe.edu.uni.fiis.universityhelper.UniversityHelper.domain.Libro;
import pe.edu.uni.fiis.universityhelper.UniversityHelper.domain.Usuario;
import pe.edu.uni.fiis.universityhelper.UniversityHelper.dto.LibroRequest;
import pe.edu.uni.fiis.universityhelper.UniversityHelper.dto.LibroResponse;
import pe.edu.uni.fiis.universityhelper.UniversityHelper.dto.UsuarioRequest;
import pe.edu.uni.fiis.universityhelper.UniversityHelper.dto.UsuarioResponse;
import pe.edu.uni.fiis.universityhelper.UniversityHelper.util.DemoUtilLibro;
import pe.edu.uni.fiis.universityhelper.UniversityHelper.util.DemoUtilUsuario;

@Service
public class MyServiceImpl implements MyService{
    @Autowired
    private MyDao myDao;


    @Override
    public LibroResponse crearLibro(LibroRequest request) {
        Libro libro=this.myDao.crearLibro(DemoUtilLibro.mapToLibro(request));
        LibroResponse response=new LibroResponse();
        response.setLibro(libro);
        return response;
    }

    @Override
    public LibroResponse eliminarLibro(LibroRequest request) {
        Libro libro=this.myDao.eliminarLibro(DemoUtilLibro.mapToLibro(request));
        LibroResponse response=new LibroResponse();
        response.setLibro(libro);
        return response;
    }

    @Override
    public UsuarioResponse crearUsuario(UsuarioRequest request) {
        Usuario usuario=this.myDao.crearUsuario(DemoUtilUsuario.mapToUsuario(request));
        UsuarioResponse response=new UsuarioResponse();
        response.setUsuario(usuario);
        return response;
    }
}
