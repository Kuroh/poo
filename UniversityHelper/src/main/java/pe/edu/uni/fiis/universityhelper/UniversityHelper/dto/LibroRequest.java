package pe.edu.uni.fiis.universityhelper.UniversityHelper.dto;

public class LibroRequest {
    private Integer coIsbn;
    private String tituloLibro;
    private String autorLibro;
    private Integer numPagLibro;
    private Integer publicLibro;
    private String descLibro;

    public Integer getCoIsbn() {
        return coIsbn;
    }

    public void setCoIsbn(Integer coIsbn) {
        this.coIsbn = coIsbn;
    }

    public String getTituloLibro() {
        return tituloLibro;
    }

    public void setTituloLibro(String tituloLibro) {
        this.tituloLibro = tituloLibro;
    }

    public String getAutorLibro() {
        return autorLibro;
    }

    public void setAutorLibro(String autorLibro) {
        this.autorLibro = autorLibro;
    }

    public Integer getNumPagLibro() {
        return numPagLibro;
    }

    public void setNumPagLibro(Integer numPagLibro) {
        this.numPagLibro = numPagLibro;
    }

    public Integer getPublicLibro() {
        return publicLibro;
    }

    public void setPublicLibro(Integer publicLibro) {
        this.publicLibro = publicLibro;
    }

    public String getDescLibro() {
        return descLibro;
    }

    public void setDescLibro(String descLibro) {
        this.descLibro = descLibro;
    }
}
