package pe.edu.uni.fiis.universityhelper.UniversityHelper.domain;

public class Usuario {
    private Integer idUsuario;
    private String nomUsuario;
    private String uniUsuario;
    private String rolUsuario;
    private String contraUsuario;
    private String tipoPagoUsuario;

    public Integer getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(Integer idUsuario) {
        this.idUsuario = idUsuario;
    }

    public String getNomUsuario() {
        return nomUsuario;
    }

    public void setNomUsuario(String nomUsuario) {
        this.nomUsuario = nomUsuario;
    }

    public String getUniUsuario() {
        return uniUsuario;
    }

    public void setUniUsuario(String uniUsuario) {
        this.uniUsuario = uniUsuario;
    }

    public String getRolUsuario() {
        return rolUsuario;
    }

    public void setRolUsuario(String rolUsuario) {
        this.rolUsuario = rolUsuario;
    }

    public String getContraUsuario() {
        return contraUsuario;
    }

    public void setContraUsuario(String contraUsuario) {
        this.contraUsuario = contraUsuario;
    }

    public String getTipoPagoUsuario() {
        return tipoPagoUsuario;
    }

    public void setTipoPagoUsuario(String tipoPagoUsuario) {
        this.tipoPagoUsuario = tipoPagoUsuario;
    }
}
