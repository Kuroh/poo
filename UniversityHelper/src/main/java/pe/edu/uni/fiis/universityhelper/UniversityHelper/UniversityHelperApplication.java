package pe.edu.uni.fiis.universityhelper.UniversityHelper;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UniversityHelperApplication {

	public static void main(String[] args) {
		SpringApplication.run(UniversityHelperApplication.class, args);
	}

}
