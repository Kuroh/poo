package pe.edu.uni.fiis.universityhelper.UniversityHelper.util;

import pe.edu.uni.fiis.universityhelper.UniversityHelper.domain.Usuario;
import pe.edu.uni.fiis.universityhelper.UniversityHelper.dto.UsuarioRequest;

public class DemoUtilUsuario {
    public static Usuario mapToUsuario(UsuarioRequest request){
        Usuario response=new Usuario();
        response.setIdUsuario(request.getIdUsuario());
        response.setContraUsuario(request.getContraUsuario());
        response.setNomUsuario(request.getNomUsuario());
        response.setRolUsuario(request.getRolUsuario());
        response.setTipoPagoUsuario(request.getTipoPagoUsuario());
        response.setUniUsuario(request.getUniUsuario());

        return response;
    }
}
