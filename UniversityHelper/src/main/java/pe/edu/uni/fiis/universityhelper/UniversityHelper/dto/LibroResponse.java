package pe.edu.uni.fiis.universityhelper.UniversityHelper.dto;

import pe.edu.uni.fiis.universityhelper.UniversityHelper.domain.Libro;

public class LibroResponse {
    private Libro libro;
    private String msgError;

    public Libro getLibro() {
        return libro;
    }

    public void setLibro(Libro libro) {
        this.libro = libro;
    }

    public String getMsgError() {
        return msgError;
    }

    public void setMsgError(String msgError) {
        this.msgError = msgError;
    }
}
