package pe.edu.uni.fiis.universityhelper.UniversityHelper.dto;

import pe.edu.uni.fiis.universityhelper.UniversityHelper.domain.Usuario;

public class UsuarioResponse {
    private Usuario usuario;
    private String msgError;

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public String getMsgError() {
        return msgError;
    }

    public void setMsgError(String msgError) {
        this.msgError = msgError;
    }
}
