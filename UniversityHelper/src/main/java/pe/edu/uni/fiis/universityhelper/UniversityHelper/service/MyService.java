package pe.edu.uni.fiis.universityhelper.UniversityHelper.service;

import pe.edu.uni.fiis.universityhelper.UniversityHelper.dto.LibroRequest;
import pe.edu.uni.fiis.universityhelper.UniversityHelper.dto.LibroResponse;
import pe.edu.uni.fiis.universityhelper.UniversityHelper.dto.UsuarioRequest;
import pe.edu.uni.fiis.universityhelper.UniversityHelper.dto.UsuarioResponse;

public interface MyService {
    LibroResponse crearLibro(LibroRequest request);
    LibroResponse eliminarLibro(LibroRequest request);
    UsuarioResponse crearUsuario(UsuarioRequest request);
}
