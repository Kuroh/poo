var app = new Vue({
    el:'#content',
    data:{
        pagina: 'register',
        identidad:null,
        titulo:null,
        autor:null,
        anio:null,
        numpag:null,
        desc:null
    },
    methods: {
        asignar : function () {
            var vm = this;
            var url='http://localhost:8080/crearLibro';
            var data = {
                "coIsbn": this.identidad,
                "tituloLibro": this.titulo,
                "autorLibro": this.autor,
                "numPagLibro": this.numpag,
                "publicLibro": this.anio,
                "descLibro": this.desc
            }
            fetch(url, {
                method: 'POST',
                body: JSON.stringify(data),
                headers: {
                    'Content-Type': 'application/json'
                }
            }).then(res => res.json())
                .catch(error => console.error('Error:', error))
                .then(response => {
                    console.log('Success:', response);
                })
        }
    }



})