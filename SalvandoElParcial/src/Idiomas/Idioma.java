package Idiomas;

public abstract class Idioma {
    protected String tipoIdioma;
    public abstract String getIdioma();
}
