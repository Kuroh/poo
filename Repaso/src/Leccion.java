import java.util.Scanner;

public class Leccion extends Alumno{

    public static Palabras palabras = new Palabras();
    public static Resumen resumen = new Resumen();

    void Add(){
        Scanner data = new Scanner(System.in);

        System.out.println("¿"+nombre+" que deseas añadir?");
        System.out.println("1: Palabras");
        System.out.println("2: Resumen");
        switch (data.nextInt()){
            case 1:{
                palabras.add();
                break;
            }
            case 2: {
                resumen.add();
                break;
            }
        }
    }

    void Show(){
        Scanner data = new Scanner(System.in);

        System.out.println("¿"+nombre+" que deseas visualizar?");
        System.out.println("1: Palabras");
        System.out.println("2: Resumen");
        System.out.println("3: Quiz");
        switch (data.nextInt()){
            case 1:{
                palabras.show();
                break;
            }
            case 2: {
                resumen.show();
                break;
            }
            case 3:{
                palabras.quiz();
                break;
            }
        }
    }

}
