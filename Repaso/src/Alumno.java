import java.util.Scanner;

public class Alumno{
    protected String nombre;
    protected String nivel;

    public String getNombre() {
        return nombre;
    }

    public String getNivel() {
        return nivel;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setNivel(String nivel) {
        this.nivel = nivel;
    }

    void Student(){
        Scanner data = new Scanner(System.in);
        System.out.println("Ingrese los datos del Alumno");
        System.out.println("Nombre: ");
        setNombre(data.nextLine());
        System.out.println("Nivel ");
        System.out.println("1: Basico");
        System.out.println("2: Intermedio");
        System.out.println("3: Avanzado");
        switch (data.nextInt()){
            case 1: setNivel("Basico");
                break;
            case 2: setNivel("Intermedio");
                break;
            case 3: setNivel("Avanzado");
                break;
        }
    }

    void Action(){
        Scanner data = new Scanner(System.in);
        Leccion leccion = new Leccion();
        int value = 1;

        do{
            System.out.println("---------------------------");
            System.out.println("¿"+nombre+" que accion desea realizar?");
            System.out.println("1: Realizar anotaciones");
            System.out.println("2: Visualizar anotaciones/Realizar un quiz");
            switch (data.nextInt()){
                case 1: leccion.Add();
                    break;
                case 2: leccion.Show();
                    break;
            }
            System.out.println("¿"+nombre+" deseas realizar otra accion?");
            System.out.println("1: Si");
            System.out.println("2: No");
            value = data.nextInt();
        }while (value==1);
    }
}
