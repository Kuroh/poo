import java.util.*;

public class Palabras extends Leccion{
    List<String> word = new ArrayList<String>();
    List<String> meaning = new ArrayList<String>();
    List<String> translation = new ArrayList<String>();
    private int counter = 0;

    void add(){
        Scanner data = new Scanner(System.in);
        System.out.println("¿Que palabra desea agregar?");
        word.add(0,data.nextLine());
        System.out.println("¿Cual es su significado?");
        meaning.add(0,data.nextLine());
        System.out.println("¿Cual es su traduccion?");
        translation.add(0,data.nextLine());

        counter++;
    }
    void show(){
        if(counter==0){
            System.out.println("No hay palabras que mostrar");
        }else{
            System.out.println("-------------------");
            for(int i=0;i<word.size();i++){
                System.out.println(word.get(i)+" / Traducción: "+translation.get(i)+" / Concepto: "+meaning.get(0));
            }
            System.out.println("-------------------");
        }
    }

    void quiz(){
        if (counter<4){
            System.out.println("Ejercicio no realizable.");
        }else{
            int keyWord=(int)(Math.random()*(word.size()));
            int anotherRandom;
            System.out.println("¿Cuál de las siguientes palabras es la traducción de "+word.get(keyWord)+"?");
            int selectorRandom=(int)(Math.random()*3);
            for(int i=0;i<4;i++){
                if(i==selectorRandom){
                    System.out.println((i+1)+": "+translation.get(keyWord));
                }else{
                    anotherRandom=(int)(Math.random()*(word.size()));
                    System.out.println((i+1)+": "+translation.get(anotherRandom));
                }
            }
        }
    }
}
