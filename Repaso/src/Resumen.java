import java.util.*;

public class Resumen extends Leccion {
    List<String> resumen = new ArrayList<String>();
    int count=0;
    void add(){
        Scanner data=new Scanner(System.in);
        System.out.println("¿Qué resumen desea agregar?");
        resumen.add(0,data.nextLine());

        count++;
    }
    void show(){
        if(count==0){
            System.out.println("No hay resumenes que mostrar.");
        }
        else{
            System.out.println("-------------------");
            System.out.println("");
            for (String resumenes:resumen){
                System.out.println(resumenes);
                System.out.println("");
            }
            System.out.println("-------------------");
        }
    }
}
