package pe.edu.fiis.tuturing.TuTuringBeta.domain;

import java.util.List;

public class Concepto extends Nivel {
    private Integer idConcepto;
    private String temaConcepto;
    public List<Contenido> contenidos;

    public List<Contenido> getContenidos() {
        return contenidos;
    }

    public void setContenidos(List<Contenido> contenidos) {
        this.contenidos = contenidos;
    }

    public Integer getIdConcepto() {
        return idConcepto;
    }

    public void setIdConcepto(Integer idConcepto) {
        this.idConcepto = idConcepto;
    }

    public String getTemaConcepto() {
        return temaConcepto;
    }

    public void setTemaConcepto(String temaConcepto) {
        this.temaConcepto = temaConcepto;
    }
}
