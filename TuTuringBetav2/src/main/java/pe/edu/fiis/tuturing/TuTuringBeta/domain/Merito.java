package pe.edu.fiis.tuturing.TuTuringBeta.domain;

public enum Merito {
    BRONZE(1,"Bronze","Aprende con paciencia",0,10),
    SILVER(2,"Silver","Tienes un gran proceso, sin embargo te falta por aprender mucho más",11,20),
    GOLD(3,"Gold","Eres un experto en el lenguaje, continúa así",21,30),
    DIAMOND(4,"Diamante","Eres un master en el lenguaje",31,50);

    private Integer coMerito;
    private String nomMerito;
    private String descMerito;
    private Integer rangMinMerito;
    private Integer rangMaxMerito;

    Merito(Integer coMerito, String nomMerito, String descMerito, Integer rangMinMerito, Integer rangMaxMerito) {
        this.coMerito = coMerito;
        this.nomMerito = nomMerito;
        this.descMerito = descMerito;
        this.rangMinMerito = rangMinMerito;
        this.rangMaxMerito = rangMaxMerito;
    }

    public Integer getCoMerito() {
        return coMerito;
    }

    public String getNomMerito() {
        return nomMerito;
    }

    public String getDescMerito() {
        return descMerito;
    }

    public Integer getRangMinMerito() {
        return rangMinMerito;
    }

    public Integer getRangMaxMerito() {
        return rangMaxMerito;
    }
}
