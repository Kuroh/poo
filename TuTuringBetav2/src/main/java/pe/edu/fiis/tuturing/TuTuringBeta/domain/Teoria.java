package pe.edu.fiis.tuturing.TuTuringBeta.domain;

public class Teoria extends Categoria {
    private Integer idTeoria;
    private String tipoAprendizaje;  //T (teoría) o P (practica)

    public Integer getIdTeoria() {
        return idTeoria;
    }

    public void setIdTeoria(Integer idTeoria) {
        this.idTeoria = idTeoria;
    }

    public String getTipoAprendizaje() {
        return tipoAprendizaje;
    }

    public void setTipoAprendizaje(String tipoAprendizaje) {
        this.tipoAprendizaje = tipoAprendizaje;
    }
}
