package pe.edu.fiis.tuturing.TuTuringBeta.dao;

import org.springframework.stereotype.Repository;
import pe.edu.fiis.tuturing.TuTuringBeta.dao.datasource.MyDatasource;
import pe.edu.fiis.tuturing.TuTuringBeta.dao.mapper.UsuarioMapper;
import pe.edu.fiis.tuturing.TuTuringBeta.domain.Usuario;
import pe.edu.fiis.tuturing.TuTuringBeta.domain.UsuarioCategoria;

@Repository
public class MyDaoImpl extends MyDatasource implements MyDao {

    @Override
    public Usuario loginUsuario(Usuario request) {
        Usuario usuario=null;
        try{
            usuario= (Usuario) this.jdbcTemplate.queryForObject("select idusuario,nomusuario,contraseniaausuario," +
                    "emailusuario from ttusuario " +
                    "where nomusuario=? and contraseniaausuario=?",new String[]{request.getNomusuario(),
                    request.getContraseniaausuario()},new UsuarioMapper());
        }catch (Exception ex){
            ex.printStackTrace();
            return null;
        }
        return usuario;
    }

    @Override
    public Usuario loginUsuarioAlternativo(Usuario request) {
        Usuario usuario=null;
        try{
            usuario= (Usuario) this.jdbcTemplate.queryForObject("select idusuario,nomusuario,contraseniaausuario," +
                    "emailusuario from ttusuario " +
                    "where nomusuario=? and emailusuario=?",new String[]{request.getNomusuario(),
                    request.getEmailusuario()},new UsuarioMapper());
        }catch (Exception ex){
            ex.printStackTrace();
            return null;
        }
        return usuario;
    }

    @Override
    public Usuario crearUsuario(Usuario request) {
        if(loginUsuario(request)!=null){
            return null;
        }
        String sql="insert into ttusuario (idusuario,nomusuario," +
                "contraseniaausuario,emailusuario) " +
                "values(?,?,?,?)";
        this.jdbcTemplate.update(sql,new Object[]{request.getIdusuario(),request.getNomusuario(),
                request.getContraseniaausuario(),request.getEmailusuario()});
        return request;
    }

    public Usuario crearUsuariov2(Usuario request){
        Usuario usuario=null;
        String sql1="select max(idusuario) idusuario,null nomusuario," +
                "             null  contraseniaausuario,null emailusuario " +
                " from ttusuario";
        usuario= (Usuario) this.jdbcTemplate.queryForObject(sql1,new UsuarioMapper());
        request.setIdusuario(usuario.getIdusuario()+1);
        return crearUsuario(request);
    }

    public Usuario actualizarContrasenia(Usuario request){
        if (loginUsuarioAlternativo(request)==null){
            return null;
        }
        String sql="update ttusuario set contraseniaausuario=? where nomusuario=? and emailusuario=?";
        this.jdbcTemplate.update(sql, new Object[]{request.getContraseniaausuario(),request.getNomusuario(),request.getEmailusuario()});
        return request;
    }

    @Override
    public Usuario crearUsuarioC(Usuario request) {
        Usuario user=loginUsuario(request);

        String sql="insert into ttusuariocategoria (nivteoria, nivpractica, idusuario, idcategoria, comerito)\n" +
                "values (0,0,?,1000,10)";
        this.jdbcTemplate.update(sql,new Object[]{user.getIdusuario()});

        return request;
    }

    @Override
    public Usuario actualizarNivelTeoriaC(Usuario request) {
        Usuario user=null;
        try{
            user=loginUsuario(request);

            String sql="update ttusuariocategoria\n" +
                    "set nivteoria = ttusuariocategoria.nivteoria+1\n" +
                    "where idusuario=? and idcategoria=1000";
            this.jdbcTemplate.update(sql,new Object[]{user.getIdusuario()});
        }catch (Exception ex){
            ex.printStackTrace();
            return null;
        }
        return request;
    }



    @Override
    public Usuario crearUsuarioJava(Usuario request) {
        Usuario user=loginUsuario(request);

        String sql="insert into ttusuariocategoria (nivteoria, nivpractica, idusuario, idcategoria, comerito)\n" +
                "values (0,0,?,1001,10)";
        this.jdbcTemplate.update(sql,new Object[]{user.getIdusuario()});

        return request;
    }

    @Override
    public Usuario actualizarNivelTeoriaJava(Usuario request) {
        Usuario user=null;
        try{
            user=loginUsuario(request);

            String sql="update ttusuariocategoria\n" +
                    "set nivteoria = ttusuariocategoria.nivteoria+1\n" +
                    "where idusuario=? and idcategoria=1001";
            this.jdbcTemplate.update(sql,new Object[]{user.getIdusuario()});
        }catch (Exception ex){
            ex.printStackTrace();
            return null;
        }
        return request;
    }

    @Override
    public Usuario actualizarNivelPracticaJava(Usuario request) {
        Usuario user=null;
        try{
            user=loginUsuario(request);

            String sql="update ttusuariocategoria\n" +
                    "set nivteoria = ttusuariocategoria.nivpractica+1\n" +
                    "where idusuario=? and idcategoria=1001";
            this.jdbcTemplate.update(sql,new Object[]{user.getIdusuario()});
        }catch (Exception ex){
            ex.printStackTrace();
            return null;
        }
        return request;
    }

    @Override
    public Usuario crearUsuarioPython(Usuario request) {
        Usuario user=loginUsuario(request);

        String sql="insert into ttusuariocategoria (nivteoria, nivpractica, idusuario, idcategoria, comerito)\n" +
                "values (0,0,?,1002,10)";
        this.jdbcTemplate.update(sql,new Object[]{user.getIdusuario()});

        return request;
    }

    @Override
    public Usuario actualizarNivelTeoriaPython(Usuario request) {
        Usuario user=null;
        try{
            user=loginUsuario(request);

            String sql="update ttusuariocategoria\n" +
                    "set nivteoria = ttusuariocategoria.nivteoria+1\n" +
                    "where idusuario=? and idcategoria=1002";
            this.jdbcTemplate.update(sql,new Object[]{user.getIdusuario()});
        }catch (Exception ex){
            ex.printStackTrace();
            return null;
        }
        return request;
    }

    @Override
    public Usuario actualizarNivelPracticaC(Usuario request) {
        Usuario user=null;
        try{
            user=loginUsuario(request);

            String sql="update ttusuariocategoria\n" +
                    "set nivteoria = ttusuariocategoria.nivpractica+1\n" +
                    "where idusuario=? and idcategoria=1000";
            this.jdbcTemplate.update(sql,new Object[]{user.getIdusuario()});
        }catch (Exception ex){
            ex.printStackTrace();
            return null;
        }
        return request;
    }

    @Override
    public Usuario actualizarNivelPracticaPython(Usuario request) {
        Usuario user=null;
        try{
            user=loginUsuario(request);

            String sql="update ttusuariocategoria\n" +
                    "set nivteoria = ttusuariocategoria.nivpractica+1\n" +
                    "where idusuario=? and idcategoria=1002";
            this.jdbcTemplate.update(sql,new Object[]{user.getIdusuario()});
        }catch (Exception ex){
            ex.printStackTrace();
            return null;
        }
        return request;
    }
}
