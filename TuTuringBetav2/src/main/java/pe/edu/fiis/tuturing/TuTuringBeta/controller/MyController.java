package pe.edu.fiis.tuturing.TuTuringBeta.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import pe.edu.fiis.tuturing.TuTuringBeta.dto.UsuarioRequest;
import pe.edu.fiis.tuturing.TuTuringBeta.dto.UsuarioResponse;
import pe.edu.fiis.tuturing.TuTuringBeta.service.MyService;

@RestController
public class MyController {

    @Autowired
    private MyService myService;

    @PostMapping(value = "/crearUsuario",consumes = MediaType.APPLICATION_JSON_VALUE)  //idUsuario creado de forma manual
    public UsuarioResponse crearUsuario(@RequestBody UsuarioRequest request){
        UsuarioResponse response = this.myService.crearUsuario(request);
        return response;
    }

    @PostMapping(value = "/crearUsuariov2", consumes = MediaType.APPLICATION_JSON_VALUE)  //idUsuario autogenerado
    public UsuarioResponse crearUsuariov2(@RequestBody UsuarioRequest request){
        UsuarioResponse response = this.myService.crearUsuariov2(request);
        return response;
    }

    @PostMapping(value = "/loginUsuario",consumes = MediaType.APPLICATION_JSON_VALUE,
                                            produces = MediaType.APPLICATION_JSON_VALUE)  //Ingresar a la página principal
    public UsuarioResponse loginUsuario(@RequestBody UsuarioRequest request){
        UsuarioResponse response = this.myService.loginUsuario(request);
        return response;
    }

    @PostMapping(value = "/actualizarContra", consumes = MediaType.APPLICATION_JSON_VALUE,
                                                produces = MediaType.APPLICATION_JSON_VALUE)  //Cambiar la contraseña de un dato actual
    public UsuarioResponse actualizarContrasenia(@RequestBody UsuarioRequest request){
        UsuarioResponse response = this.myService.actualizarContrasenia(request);
        return response;
    }

    @PostMapping(value = "/crearUsuarioC",consumes = MediaType.APPLICATION_JSON_VALUE)
    public UsuarioResponse crearUsuarioC(@RequestBody UsuarioRequest request){
        UsuarioResponse response=this.myService.crearUsuarioC(request);
        return response;
    }

    @PostMapping(value = "/actualizarNivelTeoriaC",consumes = MediaType.APPLICATION_JSON_VALUE,
                                                    produces = MediaType.APPLICATION_JSON_VALUE)
    public UsuarioResponse actualizarNivelTeoriaC(@RequestBody UsuarioRequest request){
        UsuarioResponse response=this.myService.actualizarNivelTeoriaC(request);
        return response;
    }

    @PostMapping(value = "/actualizarNivelPracticaC",consumes = MediaType.APPLICATION_JSON_VALUE,
                                                    produces = MediaType.APPLICATION_JSON_VALUE)
    public UsuarioResponse actualizarNivelPracticaC(@RequestBody UsuarioRequest request){
        UsuarioResponse response=this.myService.actualizarNivelPracticaC(request);
        return response;
    }

    @PostMapping(value = "/crearUsuarioJava",consumes = MediaType.APPLICATION_JSON_VALUE)
    public UsuarioResponse crearUsuarioJava(@RequestBody UsuarioRequest request){
        UsuarioResponse response=this.myService.crearUsuarioJava(request);
        return response;
    }

    @PostMapping(value = "/actualizarNivelTeoriaJava",consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public UsuarioResponse actualizarNivelTeoriaJava(@RequestBody UsuarioRequest request){
        UsuarioResponse response=this.myService.actualizarNivelTeoriaJava(request);
        return response;
    }

    @PostMapping(value = "/actualizarNivelPracticaJava",consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public UsuarioResponse actualizarNivelPracticaJava(@RequestBody UsuarioRequest request){
        UsuarioResponse response=this.myService.actualizarNivelPracticaJava(request);
        return response;
    }

    @PostMapping(value = "/crearUsuarioPython",consumes = MediaType.APPLICATION_JSON_VALUE)
    public UsuarioResponse crearUsuarioPython(@RequestBody UsuarioRequest request){
        UsuarioResponse response=this.myService.crearUsuarioPython(request);
        return response;
    }

    @PostMapping(value = "/actualizarNivelTeoriaPython",consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public UsuarioResponse actualizarNivelTeoriaPython(@RequestBody UsuarioRequest request){
        UsuarioResponse response=this.myService.actualizarNivelTeoriaPython(request);
        return response;
    }

    @PostMapping(value = "/actualizarNivelPracticaPython",consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public UsuarioResponse actualizarNivelPracticaPython(@RequestBody UsuarioRequest request){
        UsuarioResponse response=this.myService.actualizarNivelPracticaPython(request);
        return response;
    }
}