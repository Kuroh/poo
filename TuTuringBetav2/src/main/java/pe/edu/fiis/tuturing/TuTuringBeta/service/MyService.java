package pe.edu.fiis.tuturing.TuTuringBeta.service;

import pe.edu.fiis.tuturing.TuTuringBeta.dto.UsuarioRequest;
import pe.edu.fiis.tuturing.TuTuringBeta.dto.UsuarioResponse;

public interface MyService {
    //User Services
    UsuarioResponse crearUsuario(UsuarioRequest request);
    UsuarioResponse crearUsuariov2(UsuarioRequest request);
    UsuarioResponse loginUsuario(UsuarioRequest request);
    UsuarioResponse actualizarContrasenia(UsuarioRequest request);

    //Users per category
        //C++
    UsuarioResponse crearUsuarioC(UsuarioRequest request);
    UsuarioResponse actualizarNivelTeoriaC(UsuarioRequest request);
    UsuarioResponse actualizarNivelPracticaC(UsuarioRequest request);
        //Java
    UsuarioResponse crearUsuarioJava(UsuarioRequest request);
    UsuarioResponse actualizarNivelTeoriaJava(UsuarioRequest request);
    UsuarioResponse actualizarNivelPracticaJava(UsuarioRequest request);
        //Python
    UsuarioResponse crearUsuarioPython(UsuarioRequest request);
    UsuarioResponse actualizarNivelTeoriaPython(UsuarioRequest request);
    UsuarioResponse actualizarNivelPracticaPython(UsuarioRequest request);
}
