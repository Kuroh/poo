package pe.edu.fiis.tuturing.TuTuringBeta.domain;

public class Nivel extends Teoria {
    private Integer idNivel;
    private String tipoNivel;

    public Integer getIdNivel() {
        return idNivel;
    }

    public void setIdNivel(Integer idNivel) {
        this.idNivel = idNivel;
    }

    public String getTipoNivel() {
        return tipoNivel;
    }

    public void setTipoNivel(String tipoNivel) {
        this.tipoNivel = tipoNivel;
    }
}
