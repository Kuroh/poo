package pe.edu.fiis.tuturing.TuTuringBeta.dao;

import pe.edu.fiis.tuturing.TuTuringBeta.domain.Usuario;
import pe.edu.fiis.tuturing.TuTuringBeta.domain.UsuarioCategoria;

public interface MyDao {
    //Create, login and update users
    Usuario crearUsuario(Usuario request);
    Usuario crearUsuariov2(Usuario request);
    Usuario loginUsuario(Usuario request);
    Usuario loginUsuarioAlternativo(Usuario request);
    Usuario actualizarContrasenia(Usuario request);

    //Create users for each category
    Usuario crearUsuarioC(Usuario request);
    Usuario crearUsuarioJava(Usuario request);
    Usuario crearUsuarioPython(Usuario request);

    //Level updates
    Usuario actualizarNivelTeoriaC(Usuario request);
    Usuario actualizarNivelTeoriaJava(Usuario request);
    Usuario actualizarNivelTeoriaPython(Usuario request);
    Usuario actualizarNivelPracticaC(Usuario request);
    Usuario actualizarNivelPracticaJava(Usuario request);
    Usuario actualizarNivelPracticaPython(Usuario request);
}
