package pe.edu.fiis.tuturing.TuTuringBeta.domain;

import java.util.List;

public class Pregunta extends Contenido{
    private Integer idPregunta;
    private String textoPregunta;
    private Integer ordenPregunta;
    public List<Alternativa> alternativas;

    public List<Alternativa> getAlternativas() {
        return alternativas;
    }

    public void setAlternativas(List<Alternativa> alternativas) {
        this.alternativas = alternativas;
    }

    public Integer getIdPregunta() {
        return idPregunta;
    }

    public void setIdPregunta(Integer idPregunta) {
        this.idPregunta = idPregunta;
    }

    public String getTextoPregunta() {
        return textoPregunta;
    }

    public void setTextoPregunta(String textoPregunta) {
        this.textoPregunta = textoPregunta;
    }

    public Integer getOrdenPregunta() {
        return ordenPregunta;
    }

    public void setOrdenPregunta(Integer ordenPregunta) {
        this.ordenPregunta = ordenPregunta;
    }
}
