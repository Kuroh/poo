package pe.edu.fiis.tuturing.TuTuringBeta.dto;

import pe.edu.fiis.tuturing.TuTuringBeta.domain.Usuario;

public class UsuarioResponse {
    private Usuario usuario;
    private String msgError;

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public String getMsgError() {
        return msgError;
    }

    public void setMsgError(String msgError) {
        this.msgError = msgError;
    }
}
