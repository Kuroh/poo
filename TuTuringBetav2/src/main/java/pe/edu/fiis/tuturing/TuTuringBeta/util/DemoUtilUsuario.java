package pe.edu.fiis.tuturing.TuTuringBeta.util;

import pe.edu.fiis.tuturing.TuTuringBeta.domain.Usuario;
import pe.edu.fiis.tuturing.TuTuringBeta.dto.UsuarioRequest;

public class DemoUtilUsuario {
    public static Usuario mapToUsuario(UsuarioRequest request){
        Usuario response = new Usuario();
        response.setIdusuario(request.getIdusuario());
        response.setNomusuario(request.getNomusuario());
        response.setContraseniaausuario(request.getContraseniaausuario());
        response.setEmailusuario(request.getEmailusuario());

        return response;
    }
}
