package pe.edu.fiis.tuturing.TuTuringBeta.dto;

public class UsuarioRequest {
    private Integer idusuario;
    private String nomusuario;
    private String contraseniaausuario;
    private String emailusuario;

    public Integer getIdusuario() {
        return idusuario;
    }

    public void setIdusuario(Integer idusuario) {
        this.idusuario = idusuario;
    }

    public String getNomusuario() {
        return nomusuario;
    }

    public void setNomusuario(String nomusuario) {
        this.nomusuario = nomusuario;
    }

    public String getContraseniaausuario() {
        return contraseniaausuario;
    }

    public void setContraseniaausuario(String contraseniaausuario) {
        this.contraseniaausuario = contraseniaausuario;
    }

    public String getEmailusuario() {
        return emailusuario;
    }

    public void setEmailusuario(String emailusuario) {
        this.emailusuario = emailusuario;
    }
}
