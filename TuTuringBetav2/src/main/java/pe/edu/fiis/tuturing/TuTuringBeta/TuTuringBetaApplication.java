package pe.edu.fiis.tuturing.TuTuringBeta;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TuTuringBetaApplication {

	public static void main(String[] args) {
		SpringApplication.run(TuTuringBetaApplication.class, args);
	}

}
