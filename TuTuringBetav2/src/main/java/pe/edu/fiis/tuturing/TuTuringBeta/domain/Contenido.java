package pe.edu.fiis.tuturing.TuTuringBeta.domain;

public class Contenido {
    private Integer idContenido;
    private String tipoContenido;
    private Integer ordenContenido;
    private String textoContenido;

    public Integer getIdContenido() {
        return idContenido;
    }

    public void setIdContenido(Integer idContenido) {
        this.idContenido = idContenido;
    }

    public String getTipoContenido() {
        return tipoContenido;
    }

    public void setTipoContenido(String tipoContenido) {
        this.tipoContenido = tipoContenido;
    }

    public Integer getOrdenContenido() {
        return ordenContenido;
    }

    public void setOrdenContenido(Integer ordenContenido) {
        this.ordenContenido = ordenContenido;
    }

    public String getTextoContenido() {
        return textoContenido;
    }

    public void setTextoContenido(String textoContenido) {
        this.textoContenido = textoContenido;
    }
}
