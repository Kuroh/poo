package pe.edu.fiis.tuturing.TuTuringBeta.domain;

public class UsuarioCategoria extends Usuario{
    private Integer nivelTeoria;
    private Integer nivelPractica;
    private Integer coMerito;
    private Integer idCategoria;

    public Integer getIdCategoria() {
        return idCategoria;
    }

    public void setIdCategoria(Integer idCategoria) {
        this.idCategoria = idCategoria;
    }

    public Integer getNivelTeoria() {
        return nivelTeoria;
    }

    public void setNivelTeoria(Integer nivelTeoria) {
        this.nivelTeoria = nivelTeoria;
    }

    public Integer getNivelPractica() {
        return nivelPractica;
    }

    public void setNivelPractica(Integer nivelPractica) {
        this.nivelPractica = nivelPractica;
    }

    public Integer getCoMerito() {
        return coMerito;
    }

    public void setCoMerito(Integer coMerito) {
        this.coMerito = coMerito;
    }
}
