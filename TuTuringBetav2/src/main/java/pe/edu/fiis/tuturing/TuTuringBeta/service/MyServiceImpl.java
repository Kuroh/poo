package pe.edu.fiis.tuturing.TuTuringBeta.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pe.edu.fiis.tuturing.TuTuringBeta.dao.MyDao;
import pe.edu.fiis.tuturing.TuTuringBeta.domain.Usuario;
import pe.edu.fiis.tuturing.TuTuringBeta.dto.UsuarioRequest;
import pe.edu.fiis.tuturing.TuTuringBeta.dto.UsuarioResponse;
import pe.edu.fiis.tuturing.TuTuringBeta.util.DemoUtilUsuario;

@Service
public class MyServiceImpl implements MyService {

    @Autowired
    private MyDao myDao;

    @Override
    public UsuarioResponse crearUsuario(UsuarioRequest request) {
        Usuario usuario = this.myDao.crearUsuario(DemoUtilUsuario.mapToUsuario(request));
        UsuarioResponse response = new UsuarioResponse();
        response.setUsuario(usuario);
        return response;
    }

    public UsuarioResponse crearUsuariov2(UsuarioRequest request){
        Usuario usuario= this.myDao.crearUsuariov2(DemoUtilUsuario.mapToUsuario(request));
        UsuarioResponse response =new UsuarioResponse();
        if(usuario==null){
            response.setMsgError("Error al crear usuario. Usuario ya existente.");
        }
        response.setUsuario(usuario);
        return response;
    }

    @Override
    public UsuarioResponse loginUsuario(UsuarioRequest request) {
        Usuario usuario=this.myDao.loginUsuario(DemoUtilUsuario.mapToUsuario(request));
        UsuarioResponse response= new UsuarioResponse();
        if(usuario==null){
            response.setMsgError("Nombre de usuario / contraseña no válido o no encontrado.");
        }
        response.setUsuario(usuario);
        return response;
    }

    @Override
    public UsuarioResponse actualizarContrasenia(UsuarioRequest request) {
        Usuario usuario=this.myDao.actualizarContrasenia(DemoUtilUsuario.mapToUsuario(request));
        UsuarioResponse response= new UsuarioResponse();
        if(usuario==null){
            response.setMsgError("Usuario no válido. Verifique sus datos personales.");
        }
        response.setUsuario(usuario);
        return response;
    }

    @Override
    public UsuarioResponse crearUsuarioC(UsuarioRequest request) {
        Usuario usuario=this.myDao.crearUsuarioC(DemoUtilUsuario.mapToUsuario(request));
        UsuarioResponse response= new UsuarioResponse();
        if(usuario==null){
            response.setMsgError("Usuario no válido.");
        }
        response.setUsuario(usuario);
        return response;
    }

    @Override
    public UsuarioResponse actualizarNivelTeoriaC(UsuarioRequest request) {
        Usuario usuario=this.myDao.actualizarNivelTeoriaC(DemoUtilUsuario.mapToUsuario(request));
        UsuarioResponse response= new UsuarioResponse();
        if(usuario==null){
            response.setMsgError("Usuario no válido.");
        }
        response.setUsuario(usuario);
        return response;
    }

    @Override
    public UsuarioResponse actualizarNivelPracticaC(UsuarioRequest request) {
        Usuario usuario=this.myDao.actualizarNivelPracticaC(DemoUtilUsuario.mapToUsuario(request));
        UsuarioResponse response=new UsuarioResponse();
        if(usuario==null){
            response.setMsgError("Usuario no registrado en el curso de C++");
        }
        response.setUsuario(usuario);
        return response;
    }

    @Override
    public UsuarioResponse crearUsuarioJava(UsuarioRequest request) {
        Usuario usuario=this.myDao.crearUsuarioJava(DemoUtilUsuario.mapToUsuario(request));
        UsuarioResponse response= new UsuarioResponse();
        if(usuario==null){
            response.setMsgError("Usuario no válido.");
        }
        response.setUsuario(usuario);
        return response;
    }

    @Override
    public UsuarioResponse actualizarNivelTeoriaJava(UsuarioRequest request) {
        Usuario usuario=this.myDao.actualizarNivelTeoriaJava(DemoUtilUsuario.mapToUsuario(request));
        UsuarioResponse response= new UsuarioResponse();
        if(usuario==null){
            response.setMsgError("Usuario no válido.");
        }
        response.setUsuario(usuario);
        return response;
    }

    @Override
    public UsuarioResponse actualizarNivelPracticaJava(UsuarioRequest request) {
        Usuario usuario=this.myDao.actualizarNivelPracticaJava(DemoUtilUsuario.mapToUsuario(request));
        UsuarioResponse response=new UsuarioResponse();
        if(usuario==null){
            response.setMsgError("Usuario no registrado en el curso de Java.");
        }
        response.setUsuario(usuario);
        return response;
    }

    @Override
    public UsuarioResponse crearUsuarioPython(UsuarioRequest request) {
        Usuario usuario=this.myDao.crearUsuarioPython(DemoUtilUsuario.mapToUsuario(request));
        UsuarioResponse response= new UsuarioResponse();
        if(usuario==null){
            response.setMsgError("Usuario no válido.");
        }
        response.setUsuario(usuario);
        return response;
    }

    @Override
    public UsuarioResponse actualizarNivelTeoriaPython(UsuarioRequest request) {
        Usuario usuario=this.myDao.actualizarNivelTeoriaPython(DemoUtilUsuario.mapToUsuario(request));
        UsuarioResponse response= new UsuarioResponse();
        if(usuario==null){
            response.setMsgError("Usuario no válido.");
        }
        response.setUsuario(usuario);
        return response;
    }

    @Override
    public UsuarioResponse actualizarNivelPracticaPython(UsuarioRequest request) {
        Usuario usuario=this.myDao.actualizarNivelPracticaPython(DemoUtilUsuario.mapToUsuario(request));
        UsuarioResponse response=new UsuarioResponse();
        if(usuario==null){
            response.setMsgError("Usuario no registrado en el curso de Python.");
        }
        response.setUsuario(usuario);
        return response;
    }

}
