package pe.edu.fiis.tuturing.TuTuringBeta.dao.mapper;

import org.springframework.jdbc.core.RowMapper;
import pe.edu.fiis.tuturing.TuTuringBeta.domain.Usuario;

import java.sql.ResultSet;
import java.sql.SQLException;

public class UsuarioMapper implements RowMapper {

    @Override
    public Usuario mapRow(ResultSet resultSet, int i) throws SQLException {
        Usuario response = new Usuario();
        response.setIdusuario(resultSet.getInt("idusuario"));
        response.setNomusuario(resultSet.getString("nomusuario"));
        response.setContraseniaausuario(resultSet.getString("contraseniaausuario"));
        response.setEmailusuario(resultSet.getString("emailusuario"));

        return response;
    }
}
