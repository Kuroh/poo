package pe.edu.fiis.tuturing.TuTuringBeta.domain;

public class Alternativa {
    private Integer ordenAlternativa;
    private String textoAlternativa;

    public Integer getOrdenAlternativa() {
        return ordenAlternativa;
    }

    public void setOrdenAlternativa(Integer ordenAlternativa) {
        this.ordenAlternativa = ordenAlternativa;
    }

    public String getTextoAlternativa() {
        return textoAlternativa;
    }

    public void setTextoAlternativa(String textoAlternativa) {
        this.textoAlternativa = textoAlternativa;
    }
}
