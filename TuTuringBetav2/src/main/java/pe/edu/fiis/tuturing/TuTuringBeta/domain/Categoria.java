package pe.edu.fiis.tuturing.TuTuringBeta.domain;

import java.util.List;

public class Categoria {
    private Integer idCategoria;
    private String nomCategoria;
    public List<UsuarioCategoria> listaUsuarios;

    public List<UsuarioCategoria> getListaUsuarios() {
        return listaUsuarios;
    }

    public void setListaUsuarios(List<UsuarioCategoria> listaUsuarios) {
        this.listaUsuarios = listaUsuarios;
    }

    public Integer getIdCategoria() {
        return idCategoria;
    }

    public void setIdCategoria(Integer idCategoria) {
        this.idCategoria = idCategoria;
    }

    public String getNomCategoria() {
        return nomCategoria;
    }

    public void setNomCategoria(String nomCategoria) {
        this.nomCategoria = nomCategoria;
    }
}
