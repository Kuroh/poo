package pe.edu.fiis.test.prueba.dao;

import pe.edu.fiis.test.prueba.domain.Usuario;

import java.util.List;

public interface MyDao {
    List<Usuario> obtenerUsuarios();
    Usuario loginUsuario(Usuario request);
}
