package pe.edu.fiis.test.prueba.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import pe.edu.fiis.test.prueba.dto.UsuarioRequest;
import pe.edu.fiis.test.prueba.dto.UsuarioResponse;
import pe.edu.fiis.test.prueba.service.MyService;

@RestController
public class MyController {
    @Autowired
    private MyService myService;

    @PostMapping(value = "/showusuarios",produces = MediaType.APPLICATION_JSON_VALUE)
    public UsuarioResponse listarUsuarios(){
        UsuarioResponse response=new UsuarioResponse();
        response.setLista(this.myService.obtenerUsuarios());
        return response;
    }

    @PostMapping(value = "/usuario",consumes = MediaType.APPLICATION_JSON_VALUE,
                                    produces = MediaType.APPLICATION_JSON_VALUE)
    public UsuarioResponse loginUsuario(@RequestBody UsuarioRequest request){
        UsuarioResponse response=this.myService.loginUsuario(request);
        if(response.getUsuario()==null){
            response.setError("Usuario no encontrado");
        }
        return response;
    }
}
