package pe.edu.fiis.test.prueba.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pe.edu.fiis.test.prueba.dao.MyDao;
import pe.edu.fiis.test.prueba.domain.Usuario;
import pe.edu.fiis.test.prueba.dto.UsuarioRequest;
import pe.edu.fiis.test.prueba.dto.UsuarioResponse;
import pe.edu.fiis.test.prueba.util.DemoUtil;

import java.util.List;

@Service
public class MyServiceImpl implements MyService {

    @Autowired
    private MyDao myDao;

    @Override
    public List<Usuario> obtenerUsuarios() {
        return this.myDao.obtenerUsuarios();
    }

    @Override
    public UsuarioResponse loginUsuario(UsuarioRequest request) {
        Usuario usuario=this.myDao.loginUsuario(DemoUtil.mapToUsuario(request));
        UsuarioResponse response=new UsuarioResponse();
        response.setUsuario(usuario);
        return response;
    }
}
