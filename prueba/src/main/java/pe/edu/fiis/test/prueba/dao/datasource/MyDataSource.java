package pe.edu.fiis.test.prueba.dao.datasource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;

public abstract class MyDataSource {
    @Autowired
    protected JdbcTemplate jdbcTemplate ;
}
