package pe.edu.fiis.test.prueba.service;

import pe.edu.fiis.test.prueba.domain.Usuario;
import pe.edu.fiis.test.prueba.dto.UsuarioRequest;
import pe.edu.fiis.test.prueba.dto.UsuarioResponse;

import java.util.List;

public interface MyService {
    public List<Usuario> obtenerUsuarios();
    UsuarioResponse loginUsuario(UsuarioRequest request);
}
