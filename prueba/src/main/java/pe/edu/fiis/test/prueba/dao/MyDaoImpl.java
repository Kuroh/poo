package pe.edu.fiis.test.prueba.dao;

import org.springframework.stereotype.Repository;
import pe.edu.fiis.test.prueba.dao.datasource.MyDataSource;
import pe.edu.fiis.test.prueba.dao.mapper.UsuarioMapper;
import pe.edu.fiis.test.prueba.domain.Usuario;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Repository
public class MyDaoImpl extends MyDataSource implements MyDao {

    @Override
    public List<Usuario> obtenerUsuarios() {
        List<Usuario> lista=null;
        try{
            List<Map<String,Object>> filas=this.jdbcTemplate.queryForList(
                    "select idUsuario,credencial,nomUsuario,apellUsuario,estadoUsuario from usuario"
            );
            lista=new ArrayList<Usuario>();
            for(Map<String,Object> fila:filas){
                Usuario u=new Usuario();
                u.setIdUsuario((String) fila.get("idUsuario"));
                u.setCredencial((String) fila.get("credencial"));
                u.setNomUsuario((String) fila.get("nomUsuario"));
                u.setApellUsuario((String) fila.get("apellUsuario"));
                u.setEstadoUsuario((String) fila.get("estadoUsuario"));
                lista.add(u);
            }
        }catch (Exception ex){
            return lista;
        }
        return lista;
    }

    @Override
    public Usuario loginUsuario(Usuario request) {
        Usuario usuario=null;
        try{
            usuario= (Usuario) this.jdbcTemplate.queryForObject(
                    "select idUsuario,credencial,nomUsuario,apellUsuario,estadoUsuario from usuario where idUsuario=? and credencial=?",
                    new String[]{request.getIdUsuario(),request.getCredencial()},new UsuarioMapper());
        }catch (Exception ex){
            return usuario;
        }
        return usuario;
    }


}
