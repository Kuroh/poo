package pe.edu.fiis.test.prueba.dao.mapper;

import org.springframework.jdbc.core.RowMapper;
import pe.edu.fiis.test.prueba.domain.Usuario;

import java.sql.ResultSet;
import java.sql.SQLException;

public class UsuarioMapper implements RowMapper {
    @Override
    public Object mapRow(ResultSet resultSet, int i) throws SQLException {
        Usuario response=new Usuario();
        response.setIdUsuario(resultSet.getString("idUsuario"));
        response.setCredencial(resultSet.getString("credencial"));
        response.setNomUsuario(resultSet.getString("nomUsuario"));
        response.setApellUsuario(resultSet.getString("apellUsuario"));
        response.setEstadoUsuario(resultSet.getString("estadoUsuario"));
        return response;
    }
}
