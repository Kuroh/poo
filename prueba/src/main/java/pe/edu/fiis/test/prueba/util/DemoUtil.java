package pe.edu.fiis.test.prueba.util;

import pe.edu.fiis.test.prueba.domain.Usuario;
import pe.edu.fiis.test.prueba.dto.UsuarioRequest;

public class DemoUtil {
    public static Usuario mapToUsuario(UsuarioRequest request){
        Usuario response=new Usuario();
        response.setIdUsuario(request.getIdUsuario());
        response.setCredencial(request.getCredencial());
        response.setNomUsuario(request.getNomUsuario());
        response.setApellUsuario(request.getApellUsuario());
        response.setEstadoUsuario(request.getEstadoUsuario());
        return response;
    }
}
