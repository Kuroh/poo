package pe.edu.fiis.test.prueba.dto;

import pe.edu.fiis.test.prueba.domain.Usuario;

import java.util.List;

public class UsuarioResponse {

    private String error;
    private Usuario usuario;

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    private List<Usuario> lista;

    public List<Usuario> getLista() {
        return lista;
    }

    public void setLista(List<Usuario> lista) {
        this.lista = lista;
    }
}
